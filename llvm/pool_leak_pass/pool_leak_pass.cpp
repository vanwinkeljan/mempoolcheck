/*
 * Copyright (c) 2019 Jan Van Winkel <vanwinkeljan@gmail.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string>
#include <vector>
#include <utility>

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/CallSite.h>
#include "llvm/IR/InstIterator.h"
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>

using namespace llvm;
using namespace std;

namespace
{
class PoolLeakSanitizerPass : public ModulePass {
public:
	static char ID;
	PoolLeakSanitizerPass();

	bool runOnModule(Module &M) override;

private:
	bool wrapFunction(Module &M, const string &f_name,
			  const string &w_name);

	vector<pair<string, string> > FunctionsToWrap = {
		make_pair("pool_init", "__plsan_pool_init"),
		make_pair("pool_malloc", "__plsan_pool_malloc"),
		make_pair("pool_free", "__plsan_pool_free"),
	};
};

char PoolLeakSanitizerPass::ID = 0;
PoolLeakSanitizerPass::PoolLeakSanitizerPass() : ModulePass(ID)
{
}

bool PoolLeakSanitizerPass::runOnModule(Module &M)
{
	auto changed = false;

	for (auto &func_to_wrap : FunctionsToWrap) {
		changed |= wrapFunction(M, func_to_wrap.first,
					func_to_wrap.second);
	}

	return changed;
}

bool PoolLeakSanitizerPass::wrapFunction(Module &M, const string &f_name,
					 const string &w_name)
{
	auto changed = false;
	auto F = M.getFunction(f_name);

	if (F == nullptr) {
		errs() << "Missing function" << f_name << '\n';
		return false;
	}

	auto wF = M.getOrInsertFunction(w_name, F->getFunctionType());

	for (auto *user : F->users()) {
		Instruction *oldCall = nullptr;
		if ((oldCall = dyn_cast<Instruction>(user)) == nullptr) {
			errs() << "not an instruction" << '\n';
			continue;
		}

		CallSite call_site(oldCall);
		if (!call_site) {
			errs() << "not a call site" << '\n';
			continue;
		}

		/* Make sure the caller is not the wrapper function it
			 * self
			 * */
		if (call_site.getParent()->getParent() == wF) {
			continue;
		}

		Instruction *newInst = nullptr;
		vector<Value *> args(call_site.arg_begin(),
				     call_site.arg_end());
		if (InvokeInst *invoke = dyn_cast<InvokeInst>(oldCall)) {
			auto newInvoke =
				InvokeInst::Create(wF, invoke->getNormalDest(),
						   invoke->getUnwindDest(),
						   args, "", oldCall);
			newInvoke->setCallingConv(call_site.getCallingConv());
			newInst = cast<Instruction>(newInvoke);
		} else {
			auto newCall = CallInst::Create(wF, args, "", oldCall);
			newCall->setCallingConv(call_site.getCallingConv());
			newInst = cast<Instruction>(newCall);
		}
		newInst->setDebugLoc(oldCall->getDebugLoc());

		if (!oldCall->use_empty()) {
			oldCall->replaceAllUsesWith(newInst);
		}

		newInst->takeName(oldCall);
		oldCall->eraseFromParent();
	}

	return changed;
}

} /* namespace */

static RegisterPass<PoolLeakSanitizerPass> Y("PoolLeakSanitizerPass",
					     "Pool Leak Sanitize Pass",
					     false /* Only looks at CFG */,
					     false /* Analysis Pass */);

static RegisterStandardPasses PLSP(PassManagerBuilder::EP_ModuleOptimizerEarly,
				   [](const PassManagerBuilder &Builder,
				      legacy::PassManagerBase &PM) {
					   PM.add(new PoolLeakSanitizerPass());
				   });

static RegisterStandardPasses PLSP0(PassManagerBuilder::EP_EnabledOnOptLevel0,
				    [](const PassManagerBuilder &Builder,
				       legacy::PassManagerBase &PM) {
					    PM.add(new PoolLeakSanitizerPass());
				    });
