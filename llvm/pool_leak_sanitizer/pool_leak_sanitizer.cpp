/*
 * Copyright (c) 2019 Jan Van Winkel <vanwinkeljan@gmail.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <iostream>
#include <unordered_map>
#include <unistd.h>

#include <backward.hpp>

using namespace std;
using namespace backward;

extern "C" void pool_init(void);
extern "C" void *pool_malloc(const size_t size);
extern "C" void pool_free(void *ptr);

namespace __plsan
{
class AllocatedMemoryRegion {
public:
	AllocatedMemoryRegion(const void *base_address, const size_t size);

	const void *getBaseAddress(void) const;
	size_t getSize(void) const;
	const StackTrace& getStackTrace(void) const;

private:
	const void *base_address;
	size_t size;
	StackTrace stack_trace;
};

class PoolManager {
public:
	~PoolManager();

	void init(void);
	void *allocate(const size_t size);
	void free(void *);

	void report(void) const;
	bool hasMemmoryAllocated(void) const;

private:
	unordered_map<void *, AllocatedMemoryRegion> pool_usage;
};

static PoolManager *pool_manager;

PoolManager::~PoolManager()
{
	report();
}

void PoolManager::init(void)
{
	pool_init();
}

void *PoolManager::allocate(const size_t size)
{
	auto ptr = pool_malloc(size);

	if (ptr != nullptr) {
		auto rval = pool_usage.emplace(
			ptr, AllocatedMemoryRegion(ptr, size));
		if (!rval.second) {
			cerr << "## ERROR ## - Double allocation detected @ "
			     << ptr << " of size " << size << endl;
			StackTrace stack_trace;
			stack_trace.load_here();
			stack_trace.skip_n_firsts(3);
			Printer().print(stack_trace);
		}
	}

	return ptr;
}

void PoolManager::free(void *ptr)
{
	pool_free(ptr);
	if (pool_usage.erase(ptr) == 0) {
		cerr << "## ERROR ## - Double free detected @ " << ptr << endl;
		StackTrace stack_trace;
		stack_trace.load_here();
		stack_trace.skip_n_firsts(3);
		Printer().print(stack_trace);
	}
}

void PoolManager::report() const
{
	if (!hasMemmoryAllocated()) {
		return;
	}

	for (auto &key_val : pool_usage) {
		auto &val = key_val.second;
		cerr << "## ERROR ## - Resources leaked: " << val.getSize()
		     << " bytes leaked @ " << val.getBaseAddress() << endl;
		Printer printer;
		printer.print(val.getStackTrace());
	}
}

bool PoolManager::hasMemmoryAllocated(void) const
{
	return !pool_usage.empty();
}

AllocatedMemoryRegion::AllocatedMemoryRegion(const void *base_address,
					     const size_t size)
	: base_address(base_address), size(size), stack_trace()
{
	stack_trace.load_here();
	stack_trace.skip_n_firsts(4);
}

const void *AllocatedMemoryRegion::getBaseAddress(void) const
{
	return base_address;
}
const StackTrace& AllocatedMemoryRegion::getStackTrace(void) const
{
	return stack_trace;
}

size_t AllocatedMemoryRegion::getSize(void) const
{
	return size;
}


void init(void)
{
	pool_manager = new PoolManager;
}

void exit_check(void)
{
	int exit_code = pool_manager->hasMemmoryAllocated() ? 1 : 0;
	delete pool_manager;

	if (exit_code != 0) {
		_exit(exit_code);
	}
}

} // namespace __plsan

extern "C" void __plsan_pool_init(void)
{
	__plsan::pool_manager->init();
}

extern "C" void *__plsan_pool_malloc(const size_t size)
{
	return __plsan::pool_manager->allocate(size);
}

extern "C" void __plsan_pool_free(void *ptr)
{
	__plsan::pool_manager->free(ptr);
}

/* Make sure that __plsan::init runs before anything else by placing it in the
 * .preinit_array section
 */
__attribute__((section(".preinit_array"),
	       used)) void (*__plsan_preinit)(void) = __plsan::init;

/* Run final check at exit to detect any leaks
 */
__attribute__((section(".fini_array"),
	       used)) void (*__plsan_fini)(void) = __plsan::exit_check;
