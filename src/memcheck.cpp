/*
 * Copyright (c) 2019 Jan Van Winkel <vanwinkeljan@gmail.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <array>
#include <iostream>

#include <cstdint>
#include <cstring>

#include <sanitizer/asan_interface.h>

#define UNUSED(x) ((void)(x))

using namespace std;

namespace
{

static constexpr auto POOL_SIZE = 64 * 1024;

array<uint8_t, POOL_SIZE> pool;

struct pool_header {
	bool free;
	size_t size;
};

extern "C" void pool_init()
{
	auto header = reinterpret_cast<struct pool_header *>(pool.data());
	header->free = true;
	header->size = POOL_SIZE - 2 * sizeof(struct pool_header);

	header = reinterpret_cast<struct pool_header *>(
	    &pool.back() - sizeof(struct pool_header) + 1);
	header->free = false;
	header->size = 0;

	ASAN_POISON_MEMORY_REGION(pool.data(), POOL_SIZE);
}

auto next_pool_header(struct pool_header *header)
{
	auto byte_ptr = reinterpret_cast<uint8_t *>(header);
	byte_ptr += sizeof(struct pool_header) + header->size;
	return reinterpret_cast<struct pool_header *>(byte_ptr);
}

extern "C" void *pool_malloc(const size_t size)
{
	const auto size_with_header = size + sizeof(struct pool_header);
	auto header = reinterpret_cast<struct pool_header *>(pool.data());
	uint8_t *ptr = nullptr;

	do {
		ASAN_UNPOISON_MEMORY_REGION(header, sizeof(struct pool_header));
		if (!header->free) {
			if (header->size == 0) {
				ASAN_POISON_MEMORY_REGION(
				    header, sizeof(struct pool_header));
				break;
			}
		} else if (header->size == size) {
			header->free = false;
			ptr = reinterpret_cast<uint8_t *>(header) +
			      sizeof(struct pool_header);
		} else if (size_with_header >= header->size) {
			/* nop */
		} else {
			auto remaining_size =
			    header->size - size - sizeof(struct pool_header);
			header->free = false;
			header->size = size;

			auto next_header = next_pool_header(header);
			ASAN_UNPOISON_MEMORY_REGION(next_header,
						    sizeof(struct pool_header));
			next_header->free = true;
			next_header->size = remaining_size;
			ASAN_POISON_MEMORY_REGION(next_header,
						  sizeof(struct pool_header));

			ptr = reinterpret_cast<uint8_t *>(header) +
			      sizeof(struct pool_header);
		}
		auto new_header = next_pool_header(header);
		ASAN_POISON_MEMORY_REGION(header, sizeof(struct pool_header));
		header = new_header;

	} while (ptr == nullptr);

	if (ptr != nullptr) {
		ASAN_UNPOISON_MEMORY_REGION(ptr, size);
	}

	return ptr;
}

extern "C" void pool_free(void *ptr)
{
	auto byte_ptr = reinterpret_cast<uint8_t *>(ptr);
	byte_ptr -= sizeof(struct pool_header);
	auto header = reinterpret_cast<struct pool_header *>(byte_ptr);
	ASAN_UNPOISON_MEMORY_REGION(header, sizeof(struct pool_header));
	header->free = true;
	ASAN_POISON_MEMORY_REGION(header,
				  sizeof(struct pool_header) + header->size);
}

} // namespace

int main(int argc, char *argv[])
{
	UNUSED(argc);
	UNUSED(argv);

	pool_init();

	cout << string(80, '#') << endl
		<< "## TEST ## - Direct write in unallocated memory" << endl
		<< string(80, '#') << endl;

	*(pool.data() + POOL_SIZE/2) = 0XDE;

	cout << string(80, '#') << endl
		<< "## TEST ## - Write after free" << endl
		<< string(80, '#') << endl;

	auto alloc_size = 100;
	auto ptr = pool_malloc(alloc_size);
	memset(ptr, 0x5A, alloc_size);
	pool_free(ptr);
	memset(ptr, 0xA5, alloc_size);

	cout << string(80, '#') << endl
		<< "## TEST ## - Double free" << endl
		<< string(80, '#') << endl;
	pool_free(ptr);

	cout << string(80, '#') << endl
		<< "## TEST ## - Resource Leak" << endl
		<< string(80, '#') << endl;

	pool_malloc(10);

	return 0;
}

const char *__asan_default_options() {
	return "verbosity=1:halt_on_error=0 detect_leaks=1";
}
